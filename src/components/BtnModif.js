import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { updateStudent } from '../reduxs/students/StudentsSlice';
import ModifModal from './ModifModal';

const BtnModif = ({ student }) => {
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedStudent, setSelectedStudent] = useState(null);

  const handleEdit = () => {
    dispatch(updateStudent(selectedStudent));
  };

  const handleOpenModal = () => {
    setSelectedStudent(student);
    setModalVisible(true);
  };

  const handleCloseModal = () => {
    setSelectedStudent(null);
    setModalVisible(false);
  };

  return (
    <div>
      <button onClick={handleOpenModal}>Modifier</button>

      {modalVisible && (
        <ModifModal
          student={selectedStudent}
          onUpdateStudent={handleEdit}
          onCloseModal={handleCloseModal}
        />
      )}
    </div>
  );
};

export default BtnModif;
