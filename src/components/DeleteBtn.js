import React from 'react';
import { useDispatch } from 'react-redux';
import { deleteStudent } from '../reduxs/students/StudentsSlice';


const DeleteBtn = ({studentId}) => {
    const dispatch = useDispatch()

    const handleDelete = () => {
        dispatch(deleteStudent(studentId));
    
    }

    return (
        <div>
            <button className='btn btn-danger mx-2 shadow' onClick={handleDelete}>Suppimer</button>
        </div>
    );
}

export default DeleteBtn;

