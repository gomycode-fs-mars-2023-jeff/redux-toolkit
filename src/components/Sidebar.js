import React, { useState } from 'react';
import {
    FaTachometerAlt,
    FaBars,
    FaUserGraduate,
    FaSchool,
    FaBook,
    FaCalculator,
    FaStickyNote,
    FaChalkboardTeacher
}from "react-icons/fa";
import { NavLink } from 'react-router-dom';


const Dashboard = ({children}) => {
    const[isOpen ,setIsOpen] = useState(false);
    const toggle = () => setIsOpen (!isOpen);
    const menuItem=[
        {
            path:"/",
            name:"Dashboard",
            icon:<FaTachometerAlt/>
        },
        {
            path:"/students",
            name:"Elèves",
            icon:<FaUserGraduate/>
        },
        {
            path:"/prof",
            name:"Professeurs",
            icon:<FaChalkboardTeacher/>
        },
        {
            path:"/filiere",
            name:"Filiere",
            icon:<FaSchool/>
        },
        {
            path:"/matiere",
            name:"Matiere",
            icon:<FaBook/>
        },
        {
            path:"/note",
            name:"Note",
            icon:<FaStickyNote/>
        },
        {
            path:"/coeff",
            name:"Coefficient",
            icon:<FaCalculator/>
        }
    ]
    return (
        <div className="container">
           <div style={{width: isOpen ? "200px" : "50px"}} className="sidebar">
               <div className="top_section">
                   <h1 style={{display: isOpen ? "block" : "none"}} className="logo">Logo</h1>
                   <div style={{marginLeft: isOpen ? "50px" : "0px"}} className="bars">
                       <FaBars onClick={toggle}/>
                   </div>
               </div>
               {
                   menuItem.map((item, index)=>(
                       <NavLink to={item.path} key={index} className="link" activeclassName="active">
                           <div className="icon">{item.icon}</div>
                           <div style={{display: isOpen ? "block" : "none"}} className="link_text">{item.name}</div>
                       </NavLink>
                   ))
               }
           </div>
           <main>{children}</main>
        </div>
    );
};

export default Dashboard;