import React from 'react';
import StudentsForm from '../form/StudentsForm';
import StudentsList from '../listes/StudentsList';



const Students = () => {

    return (
        <div>
            <div>
                <StudentsForm></StudentsForm>
            </div>
            <div>
                <StudentsList></StudentsList>
            </div>
        </div>
    );
};

export default Students;